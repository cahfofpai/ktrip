KTrip is a public transport assistant targeted towards mobile Linux and Android.

It allows to query journeys for a wide range of countries/public transport providers by leveraging KPublicTransport.

It depends on Qt 5 and a number of KDE Frameworks.

Prebuilt Android apks can be found at [KDE's binary factory](https://binary-factory.kde.org/view/Android/job/KTrip_android/)